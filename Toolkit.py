class Toolkit_Obj:
    def __init__(self,delivery_kit= None,kit_size= None,terminal = None,know_how= None,trusted=None,order_level=None,storekeeper_lev=None,vehicle=None,cashless=None,exclusive=None,diccio=None):
        if diccio:
            self.delivery_kit = diccio["delivery_kit"]
            self.kit_size = diccio["kit_size"]
            self.terminal = diccio["terminal"]
            self.know_how = diccio["know_how"]
            self.trusted = diccio["trusted"]
            self.order_level = diccio["order_level"]
            self.storekeeper_lev = diccio["storekeeper_lev"]
            self.vehicle = diccio["vehicle"]
            self.cashless = diccio["cashless"]
            self.exclusive = diccio["exclusive"]
        else:
            self.delivery_kit = delivery_kit
            self.kit_size = kit_size
            self.terminal = terminal
            self.know_how = know_how
            self.trusted = trusted
            self.order_level = order_level
            self.storekeeper_lev = storekeeper_lev
            self.vehicle = vehicle
            self.cashless = cashless
            self.exclusive = exclusive

    def __str__(self):
        return str(self.delivery_kit) + str(self.kit_size) + str(self.terminal) + str(self.know_how) + str(self.trusted) + str(self.order_level) \
            + str(self.storekeeper_lev) + str(self.vehicle) + str(self.cashless) + str(self.exclusive)

    def canAttend(self,other):
        pass #comparar si es posible atender