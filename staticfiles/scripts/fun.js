function inicializar() {

    if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        map.setCenter(new GLatLng(4.6734441, -74.0484503), 17);
        map.addOverlay(new GMarker(new GLatLng(4.6734441,-74.0484503)));
        function informacion(ubicacion, descripcion) {

   var marca = new GMarker(ubicacion);
   GEvent.addListener(marca, "click", function() {
     marca.openInfoWindowHtml(descripcion); } );

     return marca;

   }

var ubicacion = new GLatLng(4.6734441,-74.0484503);
var descripcion = '<b>Microsoft</b><br/>prueba descripción<br />';
var marca = informacion(ubicacion, descripcion);

map.addOverlay(marca);
    }
}
