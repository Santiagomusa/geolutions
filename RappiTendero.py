import pygeohash
import Toolkit

class RappiTendero:
    tenderos = []
    tenderosdict = {}

    def __init__(self,serial=None,tendero_id=None,lat=None,long=None,timestamp=None,toolkit=None, diccio = None):
        if diccio:
            self.serial = diccio.get("serial")
            self.tendero_id = diccio.get("tendero_id")
            self.lat = diccio.get("lat")
            self.long = diccio.get("long")
            self.geohash = pygeohash.encode(lat, long)
            self.timestamp = diccio.get("timestamp")
            self.toolkit = Toolkit.Toolkit_Obj(diccio.get("toolkit"))
        else:
            self.serial = serial
            self.tendero_id = tendero_id
            self.lat = lat
            self.long = long
            self.geohash = pygeohash.encode(lat,long)
            self.timestamp = timestamp
            self.toolkit = toolkit
        RappiTendero.tenderos.append(self)

        if RappiTendero.tenderosdict.get(self.timestamp,default="") == "":
            RappiTendero.tenderosdict[self.timestamp] = [self]
        else:
            RappiTendero.tenderosdict[self.timestamp].append(self)

    @classmethod
    def clear(cls):
        cls.tenderos.clear()

    def canAttend(self, orden):
        return self.toolkit.canAttend(orden.toolkit)


