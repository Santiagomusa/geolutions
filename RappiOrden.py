import pygeohash
import Toolkit


class RappiOrders:
    ordenes = []
    def __init__(self, serial=None, ordenid=None, lat=None, lng=None, timestamp=None, created_at=None, tipo=None, toolkit=None,diccio:dict = None):
        if diccio!=None:
            self.diccion = diccio
            for i,j in diccio.items():
                print(i+": "+str(type(j)))
            self.serial = diccio["_id"]
            self.ordenid = diccio["id"]
            self.lat = diccio["lat"]
            self.lng = diccio["lng"]
            self.geohash = pygeohash.encode(self.lat, self.lng)
            self.timestap = diccio["timestamp"]
            self.created_at = diccio["created_at"]
            self.tipo = diccio["type"]
            self.toolkit = Toolkit.Toolkit_Obj(diccio["toolkit"])
        else:
            self.serial= serial
            self.ordenid= ordenid
            self.lat= lat
            self.lng=lng
            self.geohash = pygeohash.encode(lat,lng)
            self.timestap=timestamp
            self.created_at=created_at
            self.tipo = tipo
            self.toolkit = toolkit
        RappiOrders.ordenes.append(self)

    def __str__(self):
        return str(self.diccion)

    @classmethod
    def clear(cls):
        cls.ordenes.clear()