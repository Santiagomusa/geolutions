from pymongo import MongoClient
import RappiOrden
import pygeohash

import pprint
# cliente

# Datos
url = "mongo-hackathon.eastus2.cloudapp.azure.com"
port = "27017"
username = "hackathonmongo"
password = "hackathon2018rappimongodb"
database_name = "orders"

client = None #cliente
db = None #Database
collect = None


def connect():
    global client, db, collect
    client = MongoClient(('mongodb://'+username+':'+password+'@'+url+':'+port+'/'+database_name))
    db = client.get_database("orders")
    collect = db.get_collection("orders")


def close():
    if client:
        client.close()


def search(values: dict):

    #Recibe un diccionario como argumento
    posted = collect.find(values)
    #for obj in posted:s

    listaobj = [RappiOrden.RappiOrders(diccio=obj) for obj in posted]

    return listaobj


if __name__ == '__main__':
    connect()
    search({"type":"express"})