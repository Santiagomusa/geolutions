import psycopg2

conn = psycopg2.connect(host="postgres-hackathon.eastus2.cloudapp.azure.com", database="storekeepersdb"
                        , user="hackathonpostgres", password="hackathon2018rappipsql")
cur = None


from configparser import ConfigParser
def config(filename='database.ini', section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)

    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db

def connect():
    """ Connect to the PostgreSQL database server """
    global conn, cur
    try:
        # read connection parameters
        #params = config()


        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        #conn = psycopg2.connect(**params)

        # create a cursor
        global cur
        cur = conn.cursor()


 # execute a statement
        print('PostgreSQL database version:')
        cur.execute('SELECT version()')

        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        print(db_version)

     # close the communication with the PostgreSQL
        #cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    #finally:
    #    if conn is not None:
    #        conn.close()
    #        print('Database connection closed.')

def close():
    if cur:
        cur.close()
    if conn:
        conn.close()

if __name__ == '__main__':
    connect()
    cur.execute("SELECT * FROM storekeepers")
    print(cur.fetchall())